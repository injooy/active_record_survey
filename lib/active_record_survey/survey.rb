module ActiveRecordSurvey
	class Survey < ::ActiveRecord::Base
		self.table_name = "active_record_surveys"
		has_many :node_maps, -> { includes(:node, parent: [:node]) }, :class_name => "ActiveRecordSurvey::NodeMap", :foreign_key => :active_record_survey_id, autosave: true
		has_many :nodes, :class_name => "ActiveRecordSurvey::Node", :foreign_key => :active_record_survey_id
		has_many :questions, :class_name => "ActiveRecordSurvey::Node::Question", :foreign_key => :active_record_survey_id

		def self.trans_validation_type(type)
			type.split("::").last
		end
		def self.trans_node_type(type)
			type.split("::").last
		end
		def self.trans_simple_type_to_type(simple_type)
			if simple_type.include?("Answer") || simple_type.include?("Question")
				"ActiveRecordSurvey::Node::" + self.trans_node_type(simple_type)
			else
				"ActiveRecordSurvey::Node::Answer::" + self.trans_node_type(simple_type)
			end
		end

		# All the question nodes that follow from this survey
		def questions
			# _questions = super
			# next_question_nodes = lambda { |node|
			# 	nm.select { |node_map|
			# 		node_map.parent.nil? && node_map.node.class.ancestors.include?(::ActiveRecordSurvey::Node::Question) && !node_map.marked_for_destruction?
			# 	}.collect { |i|
			# 		# i.survey = self
			# 		# i.node.survey = self
			#    #
			# 		# list << i.node
			# 		_questions.find_by(id: i.node.id)
			# 		next_question_nodes.call(i.node)
			# 	}.flatten.uniq
			#
			# 	list
			# }
			# next_question_nodes.call(self.nodes).flatten.uniq
			# nm = self.node_maps
			# qnm = nm.select { |node_map|
			#   node_map.node.class.ancestors.include?(::ActiveRecordSurvey::Node::Question) && !node_map.marked_for_destruction?
			# }
			# qnm.each do |nm|
			#   _q = super.find_by_id(nm.active_record_survey_node_id)
			#   if _q.present?
			#     # _q[:lft] = nm.lft
			#     def _q.lft
			#       nm.lft
			#     end
			#   end
			# end
			# super.sort_by {|obj| obj[:lft]}
			super.joins(:node_maps).order("active_record_survey_node_maps.lft")
		end

		def root_node
			self.node_maps.includes(:node).select { |i| i.depth === 0 }.first
		end

		# Builds first question
		def build_first_question(question_node)
			if !question_node.class.ancestors.include?(::ActiveRecordSurvey::Node::Question)
				raise ArgumentError.new "must inherit from ::ActiveRecordSurvey::Node::Question"
			end

			question_node_maps = self.node_maps.select { |i| i.node == question_node && !i.marked_for_destruction? }

			# No node_maps exist yet from this question
			if question_node_maps.length === 0
				# Build our first node-map
				question_node_maps << self.node_maps.build(:node => question_node, :survey => self)
			end
		end

		# All the connective edges
		def edges
			self.node_maps.select { |i| !i.marked_for_destruction? }.select { |i|
				i.node && i.parent
			}.collect { |i|
				{
					:source => i.parent.node.id,
					:target => i.node.id,
				}
			}.uniq
		end

		def as_map(*args)
			options = args.extract_options!
			options[:node_maps] ||= self.node_maps

			self.node_maps.select { |i| !i.parent && !i.marked_for_destruction? }.collect { |i|
				i.as_map(options)
			}
		end
	end
end